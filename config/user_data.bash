#!/bin/bash
sh -c 'echo "deb [arch=amd64] https://apt-mo.trafficmanager.net/repos/dotnet-release/ trusty main" > /etc/apt/sources.list.d/dotnetdev.list'
apt-key adv --keyserver apt-mo.trafficmanager.net --recv-keys 417A0893
apt-get update

apt-get install dotnet-dev-1.0.0-preview2-003121 -y
apt-get install git -y
apt-get install nginx -y
apt-get install supervisor -y

ufw enable -y
ufw allow 22/tcp
ufw allow 80/tcp

mkdir /var/www
mkdir /aws-net-core
chmod 777 -R aws-net-core

export HOME="$(cd "$HOME" ; pwd)"
git clone https://gitlab.com/dbartek/aws-net-core.git
cd /aws-net-core
dotnet restore
dotnet publish

cp -r /aws-net-core/bin/Debug/netcoreapp1.0/publish /var/www/aspnetcore

/bin/cp /aws-net-core/config/nginx /etc/nginx/sites-available/default
/bin/cp /aws-net-core/config/supervisor.conf /etc/supervisor/conf.d/aspnetcore.conf

service supervisor restart
service nginx restart